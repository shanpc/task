package cn.easyutil.task.delay.handler;

import cn.easyutil.task.delay.beans.TaskDefinition;

/**
 * 延时任务重试失败后的处理
 */
public interface DelayTaskRetryErrorHandler {

    /**
     * 是否执行当前任务
     * @param definition    任务信息
     * @return 返回true 才会调用process方法
     */
    boolean supports(TaskDefinition definition);

    /**
     * 具体处理方法
     * @param definition    任务信息
     */
    void process(TaskDefinition definition);

}
