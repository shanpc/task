package cn.easyutil.task.delay.beans;

import java.io.Serializable;

/**
 * 任务详情
 */
public class TaskDefinition implements Serializable {

    private static final long serialVersionUID = 1L;

    //预期执行时间
    private Long executeTime;

    //任务内容
    private String value;

    //已经执行的次数
    private Integer executeCount;

    //当前执行任务的类
    private String taskClass;

    //错误信息
    private String error;

    //错误时间
    private String errorTime;

    //失败后延迟执行的毫秒数(默认每次重试会增加一倍的时间)
    private long retryDelayTime = 2000;

    private TaskDefinition() {}

    public static TaskDefinition create(Long executeTime, String value){
        TaskDefinition task = new TaskDefinition();
        task.setExecuteTime(executeTime);
        task.setValue(value);
        return task;
    }

    public Long getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(Long executeTime) {
        this.executeTime = executeTime;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getExecuteCount() {
        return executeCount;
    }

    public void setExecuteCount(Integer executeCount) {
        this.executeCount = executeCount;
    }

    public String getTaskClass() {
        return taskClass;
    }

    public void setTaskClass(String taskClass) {
        this.taskClass = taskClass;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorTime() {
        return errorTime;
    }

    public void setErrorTime(String errorTime) {
        this.errorTime = errorTime;
    }

    public long getRetryDelayTime() {
        return retryDelayTime;
    }

    public void setRetryDelayTime(long retryDelayTime) {
        this.retryDelayTime = retryDelayTime;
    }
}
