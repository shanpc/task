package cn.easyutil.task.delay;

import cn.easyutil.task.delay.beans.TaskDefinition;

import java.util.Collection;

public interface DelayTask {

    boolean createTask(Long executeTime,String taskValue);

    boolean createTask(TaskDefinition task);

    boolean removeTask(String...tasks);

    boolean removeTask(Collection<String> tasks);

    boolean supportsException(Exception e);

    void start();

    void stop();

    void execute(TaskDefinition definition);


}
